/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.util.ToolRunner;

/**
 *
 * @author LUK
 */
@WebServlet("/start")

public class JobRunnerServlet extends HttpServlet {


	  private static final long serialVersionUID = 1L;

	  

	  protected void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
		  String inputFile = "C:\\Users\\LUK\\Desktop\\hadoop\\prehadoop\\password.txt";
		  String[] params = new String[] { 
					"/user/vagrant/input/password.txt", 
					"/user/vagrant/output", 
					inputFile };
			try {
				int tool = ToolRunner.run(new HadoopConfig(),new ToolRunnerClass(),params);
			    PrintWriter out = response.getWriter();
				out.println("Wait for job complete:   visit http://192.168.5.10:8088 ");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    

	  }
}