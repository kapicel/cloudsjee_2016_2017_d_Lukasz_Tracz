/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author LUK
 */
public class Compute {

	public static double entropy(String s) {
		if (s.isEmpty()) {
			return 0d;
		} else {
			double entropy = 0d;
			for (int i = 0; i < 256; i++) {
				double x_prob = (StringUtils.countMatches(s, Character.toString((char)i)) / (double)s.length());
				if (x_prob > 0) {
					entropy += -x_prob * (Math.log(x_prob) / (Math.log(2)));
				}
			}
			return entropy;
		}
	}
}
